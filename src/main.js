/**
 * Created by jeroe_000 on 8-6-2016.
 */

// lets create a file that creates a div file with tags
// Initialize the main.js file
var init = () => {
    console.log("doing fine");
};

// this part doesnt work yet.
var makeContainer = (el) => {
    let containerItem = createDomeElement({tagName: 'div', attributes: {id: 'mainContainer'}});
    el.appendChild(containerItem);
}

// Code to create DomElements
var createDomeElement = (properties) => {
    // Create the element
    let domElement = document.createElement(properties.tagName);

    // Loop through the attributes to set them on the element
    let attributes = properties.attributes;
    for (let prop in attributes){
        domElement.setAttribute(prop, attributes[prop]);
    }

    // If any content, set the inner HTML
    if(properties.content){
        domElement.innerHTML = properties.content;
    }

    // Return to use in other functions
    return domElement;
}


// Actual DOM ready handler
window.addEventListener("load", init);