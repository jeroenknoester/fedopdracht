/**
 * Created by jeroe_000 on 8-6-2016.
 */
module.exports = {
    entry: './src/main.js',
    output: {
        path: 'dist',
        filename: 'bundle.js'
    },
    module:{
        loaders:[
            {test: /\.js$/, exclude:/node_modules/,loader: 'babel-loader'}
        ]
    }
}