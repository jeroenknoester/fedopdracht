# Front-end development

## Geïnstalleerd
- Babel
- Webpack
- Jquery
- Underscore
- Backbone

## Criteria
- [x] Het programma is via git opgeleverd en kent een online werkend exemplaar.

- [ ] Het programma heeft een professionele setup (bv nodejs en babeljs) en een goede mappenstructuur

- [ ] Het programma werkt in 3 browsers (Ie, Chrome en Firefox). De testresultaten zijn daarvan aanwezig.

- [ ] De code is voorzien van goed en zinvol commentaar volgens de JSDocs specificatie.

- [ ] Models (M) zijn op een goede manier gekozen en opgezet.
    Aandachtspunt: de opdracht bevat tenminste 3 models.
    
- [ ] Views (V) zijn goed gekozen en slim opgezet.
    Aandachtspunt: de opdracht uitwerking bevat tenminste 3 views
    
- [ ] Routing (*) routing van de url's is goed doordacht en uitgevoerd.

- [ ] De gekozen events werken goed en zijn zinvol opgebouwd.

- [ ] Es toepassen

- [ ] Complexiteit
